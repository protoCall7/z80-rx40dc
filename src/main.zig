const regs = @import("registers.zig");

pub fn main() void {
    systemClockInit();
    setupGPIO();

    while (true) {
        var leds_state = regs.GPIOA.ODR.read();

        regs.GPIOA.ODR.modify(.{ .ODR5 = ~leds_state.ODR5 });

        delay(60000);
    }
}

// NOTE: I think we're only running at 36mhz here -- confirm
fn systemClockInit() void {
    // flash latency = 2 wait states @ 72mhz clock
    regs.Flash.ACR.modify(.{ .LATENCY = 0b010 });

    // APB1 = HCLK/2
    regs.RCC.CFGR.modify(.{ .PPRE1 = 0b100 });

    // enable the HSI
    regs.RCC.CR.modify(.{ .HSION = 1 });
    while (regs.RCC.CR.read().HSIRDY != 1) {}

    // PLL Source = HSI / 1, x9
    regs.RCC.CFGR.modify(.{ .PLLSRC = 0b00, .PLLMUL = 0b0111 });

    // PLL On
    regs.RCC.CR.modify(.{ .PLLON = 1 });
    while (regs.RCC.CR.read().PLLRDY != 1) {}

    // Set Clock Source to PLL
    regs.RCC.CFGR.modify(.{ .SW = 0b10 });
    while (regs.RCC.CFGR.read().SWS != 0b10) {}

    // Enable GPIO A & C clocks
    regs.RCC.AHBENR.modify(.{ .IOPAEN = 1, .IOPCEN = 1 });

    // Enable SYSCFG Clock
    regs.RCC.APB2ENR.modify(.{ .SYSCFGEN = 1 });
}

fn setupGPIO() void {
    // PA5 == User LED
    // PC3 == User Button

    // PA5 = Output
    regs.GPIOA.MODER.modify(.{ .MODER5 = 0b01 });

    // PA5 Output Type = Pushpull
    regs.GPIOA.OTYPER.modify(.{ .OT5 = 0 });

    // PA5 = No Pull Up/Down
    regs.GPIOA.PUPDR.modify(.{ .PUPDR5 = 0 });

    // PA5 = Low Speed
    regs.GPIOA.OSPEEDR.modify(.{ .OSPEEDR5 = 0 });

    // PC13 = Input
    regs.GPIOC.MODER.modify(.{ .MODER13 = 0b00 });

    // PC13 = No Pull Up/Down
    regs.GPIOC.PUPDR.modify(.{ .PUPDR13 = 0 });
}

fn setupNVIC() void {
    // Tie PC13 to EXTI
    regs.SYSCFG.EXTICR4.modify(.{ .EXTI13 = 0b0010 });

    // Unmask reset line 13
    regs.EXTI.IMR1.modify(.{ .MR13 = 1 });

    // Falling edge trigger on line 13
    regs.EXTI.FTSR1.modify(.{ .TR13 = 1 });
}

fn delay(n: u32) void {
    // Sleep for some time
    var i: u32 = 0;
    while (i < n) {
        asm volatile ("nop");
        i += 1;
    }
}
