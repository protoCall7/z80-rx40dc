//
// zig-rx40dc startup code
//
// based on:
// https://github.com/rbino/zig-stm32-blink/blob/master/src/startup.zig
//

const main = @import("main.zig");

extern const _data_loadaddr: u32;
extern var _data: u32;
extern const _edata: u32;
extern const _ebss: u32;

export fn resetHandler() void {
    // Copy data from flash to RAM
    const data_loadaddr = @ptrCast([*]const u8, &_data_loadaddr);
    const data = @ptrCast([*]u8, &_data);
    const data_size = @ptrToInt(&_edata) - @ptrToInt(&_data);
    for (data_loadaddr[0..data_size]) |d, i| data[i] = d;

    // Clear the bss
    const bss = @ptrCast([*]u8, &_edata);
    const bss_size = @ptrToInt(&_ebss) - @ptrToInt(&_edata);
    for (bss[0..bss_size]) |*b| b.* = 0;

    // Call contained in main.zig
    main.main();

    unreachable;
}
